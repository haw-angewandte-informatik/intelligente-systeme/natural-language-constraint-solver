﻿Feature: AC1

@mytag
Scenario: Small AC1 consistency
	Given the first number is element of {1, 2}
	And the second number is element of {1, 2}
	And milk is element of {2, 3}
	When the first number shall be bigger than the second number
	And milk shall be bigger than the second number
	Then using AC1 it should be consistent

Scenario: Small AC1 inconsistency
	Given the Brit is element of {1, 2}
	And the Norse is element of {1, 2}
	When the Brit lives left of the Norse
	And the Norse lives left of the Brit
	Then using AC1 it should not be consistent
