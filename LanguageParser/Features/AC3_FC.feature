﻿Feature: AC3_FC

A short summary of the feature

Scenario: Small consistency
	Given the Brit is element of {1, 2}
	And the Norse is element of {1, 2}
	When the Brit lives left of the Norse
	And the Norse lives to the right of the Brit
	Then using AC3-FC there should be a solution


Scenario: Small inconsistency
	Given the Brit is element of {1, 2}
	And the Norse is element of {1, 2}
	When the Brit lives left of the Norse
	And the Norse lives left of the Brit
	Then using AC3-FC there should be no solution

@Einstein
Scenario: Big consistency
	Given 5 houses
	And one of the houses is green
	And one of the houses is blue
	And one of the houses is ivory
	And one of the houses is red
	And one of the houses is yellow
	And the Englishman lives in one of the houses
	And the Spaniard lives in one of the houses
	And the Ukrainian lives in one of the houses
	And the Norwegian lives in one of the houses
	And the Japanese lives in one of the houses
	And someone drinks orange juice
	And someone drinks milk
	And someone drinks coffee
	And someone drinks tea
	And someone drinks water
	And someone smokes Old Gold
	And someone smokes Chesterfields
	And someone smokes Kools
	And someone smokes Lucky Strike
	And someone smokes Parliaments
	And someone owns snails
	And someone owns a fox
	And someone keeps a horse
	And someone keeps a zebra
	And someone owns a dog
	When the Englishman lives in the red house
	And the Spaniard owns the dog
	And coffee is drunk in the green house
	And the Ukrainian drinks tea
	And the green house is immediately to the right of the ivory house
	And the Old Gold smoker owns snails
	And Kools are smoked in the yellow house
	And milk is drunk in the middle house
	And the Norwegian lives in the first house
	And the man who smokes the Chesterfields lives in the house next to the man with the fox
	And Kools are smoked in the house next to the house where the horse is kept
	And the Lucky Strike smoker drinks orange juice
	And the Japanese smokes Parliaments
	And the Norwegian lives next to the blue house
	Then using AC3-FC there should be a solution