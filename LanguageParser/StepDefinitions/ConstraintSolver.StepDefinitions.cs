namespace LanguageParser.StepDefinitions;

using ConstraintSolver;
using ConstraintSolver.Algorithms;
using ConstraintSolver.Helpers;

[Binding]
public sealed partial class ConstraintSolverStepDefinitions
{
    // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef
    private readonly NetWithStateManagement context = new();
    private readonly ScenarioContext scenarioContext;

    public ConstraintSolverStepDefinitions(ScenarioContext scenarioContext)
    {
        this.scenarioContext = scenarioContext;
    }

    [Given("(.+) is element of {((?:.,\\d+)*(?:.+))}")]
    public void GivenOneVariable(string variable, string numbersString)
    {
        var numbers = numbersString.Replace(" ", "").Split(",").Select(int.Parse).ToList();
        context.Variables[variable] = new(variable, numbers, new());
    }

    [When("(.+) shall be bigger than (.+)")]
    public void GivenBiggerThan(string variable1, string variable2)
    {
        context.AddConstraint(new(variable1, variable2, (i1, i2) => i1 > i2));
    }


    [Then("using AC1 it should be consistent")]
    public void ThenSolveAC1()
    {
        context.PushState();
        Consistency.AC1(context);
        Assert.IsTrue(context.IsConsistent());
    }

    [Then("using AC1 it should not be consistent")]
    public void ThenSolveNotAC1()
    {
        context.PushState();
        Consistency.AC1(context);
        Assert.IsFalse(context.IsConsistent());
    }

    [Then("using AC3-FC there should be a solution")]
    public void ThenFindSolutionAC3_FC()
    {
        var solver = new Solver(Search.AC3_FC);
        var solutionExists = solver.TrySolve(context, out var solution);
        Assert.IsTrue(solutionExists);
        Console.WriteLine("Solution found:\n"+string.Join("\n", solution!.Select(kvp => $"{kvp.Key}: {kvp.Value}")));
    }

    [Then("using AC3-FC there should be no solution")]
    public void ThenFindNoSolutionAC3_FC()
    {
        var solver = new Solver(Search.AC3_FC);
        var solutionExists = solver.TrySolve(context, out _);
        Assert.IsFalse(solutionExists);
    }

    [Then("export the net")]
    public async Task ExportConstraintNet()
    {
        var path = Path.Combine("TestResults", "net.json");
        await context.Export(path);
    }

    [Then("re-imported should also be solvable")]
    public async Task ReImportAndSolve()
    {
        var path = Path.Combine("TestResults", "net.json");
        var importedNet = await NetWithStateManagement.Import(path);
        Assert.IsNotNull(importedNet);
        var solver = new Solver(Search.AC3_FC);
        Assert.IsTrue(solver.TrySolve(importedNet, out _));
    }
}