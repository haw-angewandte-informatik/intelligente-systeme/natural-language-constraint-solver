﻿namespace LanguageParser.StepDefinitions
{
    using System.Configuration;
    using TechTalk.SpecFlow;
    
    public sealed partial class ConstraintSolverStepDefinitions
    {
        // ConstraintNet in general file ConstraintSolver.StepDefinitions.cs

        #region [ Variables ]

        private int NumberOfHouses = 5;
        private List<int> PossibleValues => Enumerable.Range(1, NumberOfHouses).ToList();

        [Given("(\\d+) houses")]
        public void SetNumberOfHouses(int numberOfHouses)
        {
            NumberOfHouses = numberOfHouses;
        }

        private readonly List<string> PersonVariables = new();
        [Given("(?:the |a )?(.+) lives in one of the houses")]
        public void DeclarePerson(string person)
        {
            context.Variables[person] = new(person, PossibleValues, new());
            PersonVariables.Add(person);
        }

        private readonly List<string> ColourVariables = new();
        [Given("one of the houses is (.+)")]
        public void DeclareColour(string colour)
        {
            context.Variables[colour] = new(colour, PossibleValues, new());
            ColourVariables.Add(colour);
        }

        private readonly List<string> DrinkVariables = new();
        [Given("someone drinks (.+)")]
        public void DeclareDrink(string drink)
        {
            context.Variables[drink] = new(drink, PossibleValues, new());
            DrinkVariables.Add(drink);
        }

        private readonly List<string> SmokeVariables = new();
        [Given("someone smokes (.+)")]
        public void DeclareSmokes(string brand)
        {
            context.Variables[brand] = new(brand, PossibleValues, new());
            SmokeVariables.Add(brand);
        }

        private readonly List<string> PetVariables = new();
        [Given("someone keeps(?: the| a)? (.+)")]
        [Given("someone owns(?: the| a)? (.+)")]
        public void DeclarePet(string pet)
        {
            context.Variables[pet] = new(pet, PossibleValues, new());
            PetVariables.Add(pet);
        }

        #endregion

        #region [ Constraints ]

        [AfterScenarioBlock("Einstein")]
        public void AddUnequalityConstraints()
        {
            if (scenarioContext.CurrentScenarioBlock != ScenarioBlock.Given) return;

            foreach (var listOfVars in new[]{PersonVariables, DrinkVariables, PetVariables, SmokeVariables, ColourVariables})
            {
                for(var idx = 0; idx < listOfVars.Count-1; idx++)
                {
                    for (var other = 0; other < listOfVars.Count; other++)
                    {
                        if(other != idx)
                            context.AddConstraint(new(listOfVars[idx], listOfVars[other], (v, o) => v != o));
                    }
                }
            }
        }

        [When("(.+) lives left of (.+)")]
        [When("(.+) lives to the left of (.+)")]
        public void GivenPersonLeftOfPerson(string leftNeighbour, string rightNeihgbour)
        {
            context.AddConstraint(new(leftNeighbour, rightNeihgbour, (l, r) => l == r - 1));
        }

        [When("(.+) lives right of (.+)")]
        [When("(.+) lives to the right of (.+)")]
        public void GivenPersonRightOfPerson(string rightNeihgbour, string leftNeighbour)
        {
            context.AddConstraint(new(rightNeihgbour, leftNeighbour, (r, l) => r == l + 1));
        }

        [When("(?:the |a )?([a-zA-Z ]+) lives in the ([a-zA-Z ]+) house")]
        [When("(?:the |a )?([a-zA-Z ]+) owns(?: the| a)? ([a-zA-Z ]+)")]
        [When("([a-zA-Z ]+) is drunk in the ([a-zA-Z ]+) house")]
        [When("(?:the |a )?([a-zA-Z ]+) drinks ([a-zA-Z ]+)")]
        [When("([a-zA-Z ]+) are smoked in the ([a-zA-Z ]+) house")]
        [When("(?:the |a )?([a-zA-Z ]+) smokes(?! the| a) ([a-zA-Z ]+)")]
        public void GivenOneThingMatchesAnotherThing(string first, string second)
        {
            if (first.EndsWith("smoker")) // Regex will nicht
            {
                first = first.Remove(first.LastIndexOf(' '));
            }

            if(context.Variables.ContainsKey(second))
            {
                context.AddConstraint(new(first, second, (f, s) => f == s));
            }
            else
            {
                AddUnaryEquality(first, second);
            }
        }

        private void AddUnaryEquality(string variable, string position)
        {
            var pos = position.ToLower() switch
            {
                "first" => 1,
                "second" => 2,
                "third" => 3,
                "fourth" => 4,
                "fifth" => 5,
                "middle" => (NumberOfHouses+1)/2,
                "last" => NumberOfHouses,
                _ => throw new ArgumentOutOfRangeException($"Couldn't parse a position from '{position}'")
            };

            context.UnaryConstraints.Add((variable, val => val == pos));
        }

        [When("the ([a-zA-Z ]+) house is immediately to the left of the ([a-zA-Z ]+) house")]
        public void WhenColourImmediatelyLeftOfColour(string leftColour, string rightColour)
        {
            context.AddConstraint(new(leftColour, rightColour, (l, r) => l == r-1));
        }

        [When("the ([a-zA-Z ]+) house is immediately to the right of the ([a-zA-Z ]+) house")]
        public void WhenColourImmediatelyRightOfColour(string rightColour, string leftColour)
        {
            // Just reverse order
            WhenColourImmediatelyLeftOfColour(leftColour, rightColour);
        }

        [When("the man who smokes the ([a-zA-Z ]+) lives in the house next to the man with the ([a-zA-Z ]+)")]
        [When("([a-zA-Z ]+) are smoked in the house next to the house where the ([a-zA-Z ]+) is kept")]
        [When("the ([a-zA-Z ]+) lives next to the ([a-zA-Z ]+) house")]
        public void WhenOneNextToOther(string first, string second)
        {
            context.AddConstraint(new(first, second, (f, s) => Math.Abs(f-s) == 1));
        }

        #endregion
    }
}