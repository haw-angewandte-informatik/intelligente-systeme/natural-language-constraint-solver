namespace ConstraintSolver.Helpers;

using System.Runtime.CompilerServices;

public static class DictionaryExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static TValue GetOrCreate<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key)
        where TValue : new()
        where TKey : notnull
    {
        if (self.TryGetValue(key, out TValue? value))
        {
            return value;
        }

        TValue newVal = new();

        self[key] = newVal;

        return newVal;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static TValue GetOrCreate<TKey, TValue>(this Dictionary<TKey, TValue> self, TKey key, Func<TValue> creator)
        where TKey : notnull
    {
        if (self.TryGetValue(key, out TValue? value))
        {
            return value;
        }

        TValue newVal = creator();

        self[key] = newVal;

        return newVal;
    }
}