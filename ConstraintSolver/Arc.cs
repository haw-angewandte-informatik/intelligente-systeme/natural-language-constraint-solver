﻿namespace ConstraintSolver;
using System.Collections.Generic;

public record Arc(Variable VarA, Variable VarB, List<BinaryConstraint> Constraints);