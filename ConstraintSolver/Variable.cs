﻿namespace ConstraintSolver;

using System.Collections.Generic;

public record Variable(string Name, List<int> Values, Dictionary<string, Arc> Arcs)
{
    public override int GetHashCode()
    {
        return Name.GetHashCode();
    }

    public virtual bool Equals(Variable? other)
    {
        return this.Name == other?.Name;
    }
}