﻿namespace ConstraintSolver.Algorithms;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Search
{
    public static SearchAlgorithm AC3_FC = context =>
    {
        // 1. Zuerst Kantenkonsistenz
        context.PushState();
        Consistency.AC1(context);
        if (!context.IsConsistent())
        {
            return null;
        }

        // (2. Optional: Variablen in günstige Reihenfolge bringen)
        var vars = context.Variables.Values.ToList();
        var solution = new Dictionary<string, int>();

        // 3. Zustand speichern auf Zustands-Stack
        context.PushState();

        var konsistent = ac3_fc_rec(0);

        bool ac3_fc_rec(int cv)
        {
            var possibleValues = vars[cv].Values.ToArray();
            foreach (var possibleValue in possibleValues)
            {
                // 4. Annahme für nächste Variable entsprechend Sortierung treffen
                context.PushState();
                solution[vars[cv].Name] = possibleValue; // Potenziellen Wert in Lösung schreiben
                for (var i = vars[cv].Values.Count - 1; i >= 1; i--) // Alle anderen aus den möglichen Werten löschen
                {
                    context.Delete(vars[cv], possibleValue);
                }

                bool endReached = cv + 1 > context.Variables.Count-1;

                // 5. Kantenkonsistenz ab der aktuellen Variablen herstellen
                konsistent = ac3_Fc(cv) && (endReached || ac3_fc_rec(cv+1));


                if (konsistent)
                    return true;

                context.PopState();
                solution.Remove(vars[cv].Name);
            }

            return false;
        }

        // procedure AC3-FC(cv)
        bool ac3_Fc(int idx)
        {
            var currentVar = vars[idx]; // Zuletzt instanziierte Variable
            Queue<Arc> q = new(vars[idx].Arcs.Where((k_v)=> vars.IndexOf(context.Variables[k_v.Key]) > idx).Select(k_v => k_v.Value).ToList());
            var consistent = true;
            while (q.TryDequeue(out var currentArc) && consistent)
            {
                if (Consistency.Revise(context, currentArc))
                {
                    consistent = currentVar.Values.Any();
                }
            }

            return consistent;
        }

        return konsistent
            ? solution
            : null;
    };
}