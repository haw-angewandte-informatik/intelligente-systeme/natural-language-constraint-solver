﻿namespace ConstraintSolver.Algorithms;

public static class Consistency
{
    public static readonly ConsistencyAlgorithm AC1 = context =>
    {
        var change = false;
        do
        {
            change = false;
            foreach (var variable in context.Variables.Values)
            {
                foreach (var arc in variable.Arcs.Values)
                {
                    change = Revise(context, arc) || change;
                }
            }
        } while (change);
    };

    /// <summary>
    /// Filtert die Werte der gegebenen Variablen anhand von binären Constraints bezogen auf eine andere Variable.
    /// Es werden beide Richtungen geprüft, also sowohl "v1 == v2" als auch "v2 == v1".
    /// </summary>
    /// <param name="context">Das Constraintnetz auf dem gearbeitet wird.</param>
    /// <param name="arc"></param>
    /// <returns>Gibt an, ob die Liste möglicher Werte verringert wurde.</returns>
    internal static bool Revise(
        NetWithStateManagement context,
        Arc arc)
    {
        // Alle Constraints finden, die für die zwei gegebenen Variablen gelten
        var currentConstraints = arc.Constraints;

        // Constraints anwenden
        var currentVar1 = arc.VarA.Name;
        var compareTo1 = arc.VarB.Name;

        var deleted = false;
        for (var i = context.Variables[currentVar1].Values.Count - 1; i >= 0; i--)
        {
            if (!IsValidValue(context.Variables[currentVar1], context.Variables[compareTo1], currentConstraints, context.Variables[currentVar1].Values[i]))
            {
                context.Delete(context.Variables[currentVar1], context.Variables[currentVar1].Values[i]);
                deleted = true;
            }
        }

        return deleted;
    }

    private static bool IsValidValue(Variable currentVar, Variable compareTo, IReadOnlyCollection<BinaryConstraint> currentConstraints, int val)
    {
        return currentConstraints.All(c => compareTo.Values.Any(other => currentVar.Name == c.First
            ? c.Predicate(val, other)
            : c.Predicate(other, val)));
    }
}

public delegate void ConsistencyAlgorithm(NetWithStateManagement context);