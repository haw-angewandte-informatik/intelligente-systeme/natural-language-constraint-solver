﻿namespace ConstraintSolver;

using System;

public record BinaryConstraint(string First, string Second, Func<int, int, bool> Predicate)
{
    public bool ConnectsVariables(string oneVar, string otherVar) =>
        (First.Equals(oneVar) && Second.Equals(otherVar)) ||
        (First.Equals(otherVar) && Second.Equals(oneVar));
}