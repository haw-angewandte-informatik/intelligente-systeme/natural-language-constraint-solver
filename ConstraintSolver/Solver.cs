﻿namespace ConstraintSolver;

using System.Runtime.CompilerServices;

public delegate Dictionary<string, int>? SearchAlgorithm(NetWithStateManagement constraintNet);

public class Solver
{
    private readonly SearchAlgorithm algorithm;

    public Solver(SearchAlgorithm algo)
    {
        this.algorithm = algo;
    }

    public bool TrySolve(NetWithStateManagement inputNet, out Dictionary<string, int>? solution)
    {
        // Originales Constraintnetz nicht verändern, also Daten übernehmen
        // Dabei direkt unäre Constraints prüfen
        var context = new NetWithStateManagement
        {
            Variables = FilterByUnaryConstraints(inputNet.Variables, inputNet.UnaryConstraints),
        };

        // Ist es durch die unären Constraints bereits unmöglich geworden, das Netz zu lösen, sofort abbrechen
        if (context.Variables.Any(v => !v.Value.Values.Any()))
        {
            solution = new Dictionary<string, int>(0);
            return false;
        }

        // Eigentlichen Algorithmus anwenden
        var solvedVariables = algorithm(context);

        // False und leere Lösung, falls es keine Lösung gibt
        if (solvedVariables == null)
        {
            solution = null;
            return false;
        }

        solution = solvedVariables;
        return true;
    }

    /// <summary>
    /// Filters a passed list of variables with possible values against a list of constraints.
    /// </summary>
    /// <param name="variables">Variables, each with a list of its possible values.</param>
    /// <param name="unaryConstraints">List of unary constraints, each with the name if the variable it belongs to.</param>
    /// <returns>A new dictionary of all variables, each with the values possible with the given constraints.</returns>
    protected Dictionary<string, Variable> FilterByUnaryConstraints(
        in Dictionary<string, Variable> variables,
        List<(string Variable, Predicate<int> Constraint)> unaryConstraints)
    {
        return variables.ToDictionary( // Nimm alle Variablen
            pair => pair.Key,
            pair => new Variable(pair.Key, pair.Value.Values.Where(val => // Aber von allen möglichen Werten/Belegungen einer Variablen
                        unaryConstraints
                            .Where(c =>
                                c.Variable.Equals(pair
                                    .Key)) // nimm alle unären Constraints die für diese Variable gelten
                            .All(c => c.Constraint(val))) // und nimm nur die Werte die all diese Constraints erfüllen.
                    .ToList(),
                pair.Value.Arcs)
        );
    }
}