﻿namespace ConstraintSolver;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.Json;
using System.Text.Json.Serialization;
using Helpers;

public class ConstraintNet
{
    public Dictionary<string, Variable> Variables = new();
    public List<(string Variable, Predicate<int> Constraint)> UnaryConstraints = new();

    internal static JsonSerializerOptions SerializerOptions => new()
    {
        IncludeFields = true,
        ReferenceHandler = ReferenceHandler.Preserve
    };

    /// <summary>
    /// Ist das Netz in seiner aktuellen Form kantenkonsistent?
    /// </summary>
    /// <returns></returns>
    public bool IsConsistent() => Variables.Values.All(v => v.Values.Any());

    public void AddConstraint(BinaryConstraint constraint)
    {
        try
        {
            Variables[constraint.First].Arcs
                .GetOrCreate(Variables[constraint.Second].Name,
                    () => new(Variables[constraint.First], Variables[constraint.Second], new()))
                .Constraints.Add(constraint);
            Variables[constraint.Second].Arcs
                .GetOrCreate(Variables[constraint.First].Name,
                    () => new(Variables[constraint.Second], Variables[constraint.First], new()))
                .Constraints.Add(constraint);
        }
        catch (KeyNotFoundException exception)
        {
            throw new InvalidOperationException($"Not a variable: {exception.Message}", exception);
        }
    }

    public async Task Export(string path)
    {
        
        await File.WriteAllTextAsync(path, JsonSerializer.Serialize(this, SerializerOptions));
    }
}




public class NetWithStateManagement : ConstraintNet
{

    public static async Task<NetWithStateManagement?> Import(string path) =>
        await JsonSerializer.DeserializeAsync<NetWithStateManagement>(File.OpenRead(path),
            SerializerOptions);

    private record Deletion(IList<int> List, int Deleted)
    {
        public void Undo() => List.Add(Deleted);
    }

    private record Changes(List<Deletion> ChangeList)
    {
        public void Undo() => ChangeList.ForEach(c => c.Undo());
    }

    private readonly Stack<Changes> changeStack = new();

    public void PushState()
    {
        changeStack.Push(new Changes(new List<Deletion>()));
    }

    public void Delete(Variable v, int item)
    {
        v.Values.Remove(item);

        changeStack.Peek().ChangeList.Add(new Deletion(v.Values, item));
    }

    public void PopState()
    {
        changeStack.Pop().Undo();
    }
}